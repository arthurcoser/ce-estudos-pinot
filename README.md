# Ce Estudos Pinot

## Início

### Variáveis de ambiente

Copie o arquivo `.env.default` para `.env`

```
cp .env.default .env
```

Preencha as variáveis com valores desejados

### Copie arquivos csv originais para ingestão no Apache Pinot

```
mv path/to/escolas_br_ativas_geo_2020.csv pinot_data/culturaeduca/escolas_br_ativas_geo/2020/rawdata/escolas_br_ativas_geo_2020.csv

mv path/to/SP_Setores_2020.csv pinot_data/culturaeduca/setores_censitarios/2020/rawdata/SP_Setores_2020.csv
```

### Connector trino - postgresql

Copie o arquivo `trino_etc/catalog/postgresql.properties.default` para `trino_etc/catalog/postgresql.properties`

```
cp trino_etc/catalog/postgresql.properties.default trino_etc/catalog/postgresql.properties
```

Preencha as variáveis com valores referentes aos valores de .env

## Uso

### Docker compose

Iniciar docker compose utilizando o arquivo .env:

```
docker compose --env-file=.env up -d
```

Para parar os containers

```
docker compose stop
```

Para remover os containers

```
docker compose down
```

### Popular Apache Pinot

Adiciona tabela escolas

```
sh scripts/pinot/addtable-escolas.sh
```

Ingestão de dados de escolas

```
sh scripts/pinot/launchdataingestionjob-escolas.sh
```

Adiciona tabela setores

```
sh scripts/pinot/addtable-setores.sh
```

Ingestão de dados de setores

```
sh scripts/pinot/launchdataingestionjob-setores.sh
```

### Trino

Para acessar a CLI do Trino:

```
docker exec -it ce-trino trino
```
