#! bin/sh

docker exec -it ce-pinot-controller bin/pinot-admin.sh AddTable \
  -tableConfigFile /opt/pinot/data/culturaeduca/setores_censitarios/2020/table-offline.json \
  -schemaFile /opt/pinot/data/culturaeduca/setores_censitarios/2020/schema.json \
  -exec
