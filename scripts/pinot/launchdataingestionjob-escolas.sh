#! bin/sh

docker exec -it ce-pinot-controller bin/pinot-admin.sh LaunchDataIngestionJob \
  -jobSpecFile /opt/pinot/data/culturaeduca/escolas_br_ativas_geo/2020/batch-job-spec.yml