#! bin/sh

docker exec -it ce-pinot-controller bin/pinot-admin.sh AddTable \
  -tableConfigFile /opt/pinot/data/culturaeduca/escolas_br_ativas_geo/2020/table-offline.json \
  -schemaFile /opt/pinot/data/culturaeduca/escolas_br_ativas_geo/2020/schema.json \
  -exec
