#! bin/sh

docker exec -it ce-pinot-controller bin/pinot-admin.sh LaunchDataIngestionJob \
  -jobSpecFile /opt/pinot/data/culturaeduca/setores_censitarios/2020/batch-job-spec.yml